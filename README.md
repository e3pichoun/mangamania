INITIAL: 13 Janvier 2021

Voici le projet Mangamania, un projet qui sera celui que je présenterai pour la validation du diplôme "Devellopeur Web et Web mobile" de Simplon Le Cheylard.

Le thème de son projet est dans le titre, cela parlera de manga (Bande déssinée japonaise) et d'animé (Dessin animé japonaise).

Le but espéré de ce projet est de créer un site media où on pourrait partager les actualités, les dossiers écrits, rechercher des mangas et animés en autres.

Le site serait répartie comme ceci: Accueil / Rechercher / Actualités / Dossiers / À Propos


Voilà les prèmieres tâches que j'ai pu dégagé du projet

OBLIGATOIRE - Créer un site avec des pages interconnnectées

OBLIGATIORE - Que le site soit reponsible (soit qu'il soit utilisable quelque soit le support utilisé)

OBLIGATIORE - Utiliser la base de donnée pour la connexion des membres du site tout en respectant la norme RGPD (Réforme Européene sur la protection des données)

FALCULTATIVE - Mettre un carouel pour la partie Actualité (et de la partie Dossiers) de la main page pour avoir un max de contenu

FALCULTATIVE - Utiliser des APIs pour rechercher les mangas et les animés
    ALTERNATIVE - Utiliser une base de donnée

EXTRA - Faire une zone forum pour débatre de l'actualité ou des dossiers sorties


Le projet sera fait en HTML 5 / CSS 3 / JavaScript / PHP / SQL en autres et potentiellement sera migré vers un CMS comme WordPress ou Joomla!

Voilà les bases annoncées pour le projet, il aura surement des updates pour ajouter des tâches (obligatoires comme falcultatives) et pour annoncer l'avancement des tâches accomplis.

Yohann Cadet - Gérant de Mangamania

----------


